#!/usr/bin/env python

"""
Gather heavy and light chain AA sequences from figure S1 into a CSV file.
"""

import re
import sys
from collections import defaultdict
from csv import DictWriter

def seqdict(path, ungap=True):
    seqs = defaultdict(str)
    seqid = None
    with open(path) as f_in:
        for line in f_in:
            line = line.strip()
            if line.startswith(">"):
                seqid = line[1:]
            else:
                seqs[seqid] += line
            if ungap:
                seqs[seqid] = seqs[seqid].replace("-", "")
    return seqs

def make_parsed_sheet(path_heavy, path_light, path_out):
    heavy = seqdict(path_heavy)
    light = seqdict(path_light)
    rows = []
    for key in set(heavy.keys()) | set(light.keys()):
        row = {
            "Entry": key,
            "HeavyAA": heavy.get(key),
            "LightAA": light.get(key)}
        if row["Entry"] == "GL":
            row["Category"] = "germline"
        elif row["Entry"].startswith("PGT"):
            row["Category"] = "original-mab"
        else:
            row["Category"] = "new-mab"
        rows.append(row)
    def intish(txt):
        try:
            return int(re.sub(".*[^0-9]", "", txt))
        except ValueError:
            return 0
    rows.sort(key=lambda r: (r["Category"], intish(r["Entry"]), r["Entry"]))
    with open(path_out, "w") as f_out:
        writer = DictWriter(f_out, ["Entry", "Category", "HeavyAA", "LightAA"], lineterminator="\n")
        writer.writeheader()
        writer.writerows(rows)

if __name__ == "__main__":
    make_parsed_sheet(sys.argv[1], sys.argv[2], sys.argv[3])
