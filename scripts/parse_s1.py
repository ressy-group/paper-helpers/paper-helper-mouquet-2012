#!/usr/bin/env python

import re
import sys

def parse_s1(path_txt, path_fasta):
    with open(path_txt) as f_in, open(path_fasta, "w") as f_out:
        for line in f_in:
            seqid, seq = re.match(r"([^\s]+)\s+([^\s]+)\s*", line).groups()
            if seqid != "consensus":
                f_out.write(f">{seqid}\n{seq}\n")

if __name__ == "__main__":
    parse_s1(sys.argv[1], sys.argv[2])
