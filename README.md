# Antibody sequences from Mouquet 2012

PGT121-123-lineage antibody and partially-reverted germline AA sequences from:

Complex-type N-glycan recognition by potent broadly neutralizing HIV antibodies.
Mouquet et al.
PNAS Immunology and Inflammation 109 (47) E3268-E3277
<https://doi.org/10.1073/pnas.1217207109>

PDB:

 * 4FQ1: unliganded PGT121 Fab
 * 4FQC: “liganded” PGT121 Fab
 * 4FQ2: 10-1074 Fab
 * 4FQQ: GL Fab
