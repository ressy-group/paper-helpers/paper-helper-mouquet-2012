# alignments of heavy and light chain mAbs and GL sequences
panels = S1A S1B
s1out = $(addsuffix .fa,$(addprefix parsed/,$(panels)))
# GL, 10-1074, liganded PGT-121, unliganded PGT-121
pdb = $(addsuffix .fa,$(addprefix from-pdb/,4FQQ_A 4FQQ_B 4FQ2_H 4FQ2_L 4FQC_H 4FQC_L 4FQ1_H 4FQ1_L))

.SECONDARY:

all: output/seqs.csv $(pdb)

output/seqs.csv: $(s1out)
	python scripts/make_parsed_sheet.py $^ $@

from-pdb/%.fa:
	python scripts/download_ncbi.py protein $* $@

parsed/%.fa: from-paper/fig%.txt
	python scripts/parse_s1.py $^ $@
